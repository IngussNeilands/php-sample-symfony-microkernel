<?php

namespace Sample\Symfony\Controller {

    use Symfony\Bundle\FrameworkBundle\Controller\Controller;

    class HomeController extends Controller {

        public function indexAction() {

            return $this->render('@Sample/index.twig', ['message' => 'Hello World!']);
        }

    }

}
