<?php

namespace Sample\Symfony {

    use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
    use Symfony\Component\Config\Loader\LoaderInterface;
    use Symfony\Component\DependencyInjection\ContainerBuilder;
    use Symfony\Component\HttpKernel\Kernel;
    use Symfony\Component\Routing\RouteCollectionBuilder;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\Debug\Debug;

    class MicroKernel extends Kernel {

        use MicroKernelTrait;

        public function __construct($environment, $debug) {

            if (true == $debug):

                error_reporting(E_ALL ^ E_NOTICE);
                ini_set('display_errors', 1);
                Debug::enable();

            endif;

            parent::__construct($environment, $debug);

            $this->rootDir = dirname($this->rootDir);
        }

        public function registerBundles() {

            $bundles = array(
                new \Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
                new \Symfony\Bundle\TwigBundle\TwigBundle(),
                new \Sample\Symfony\AppBundle(),
            );

            return $bundles;
        }

        protected function configureRoutes(RouteCollectionBuilder $routes) {

            $routes->import($this->rootDir . '/app/Config/routes.yml');
        }

        protected function configureContainer(ContainerBuilder $c, LoaderInterface $loader) {

            $loader->load($this->rootDir . '/app/Config/config.' . $this->getEnvironment() . '.yml');
            $loader->load($this->rootDir . '/app/Config/services.yml');
        }

        public function getLogDir() {

            return $this->rootDir . '/tmp/';
        }

        public function getCacheDir() {

            return $this->rootDir . '/tmp/' . $this->environment;
        }

        public function run() {

            $request = Request::createFromGlobals();

            $response = $this->handle($request);
            $response->send();

            $this->terminate($request, $response);
        }

    }

}
