<?php

namespace Sample\Symfony\Twig {

    class MicroTimeExtension extends \Twig_Extension {

        public function getFunctions() {
            return [
                new \Twig_SimpleFunction('getMicroTime', [$this, 'getMicroTime'], ['is_safe' => ['html']]),
            ];
        }

        public function getMicroTime() {
            return round((microtime(true) - MICROTIME) * 1000, 2);
        }

        public function getName() {
            return 'app_extension';
        }

    }

}
