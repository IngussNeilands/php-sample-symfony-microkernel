<?php

define('MICROTIME', microtime(true));

error_reporting(E_ALL ^ E_NOTICE);
ini_set('display_errors', 1);

require dirname(__DIR__) . '/vendor/autoload.php';

if (empty($_SERVER['VAGRANT'])):
    $app = new \Sample\Symfony\MicroKernel('live', false);
else:
    #$app = new \Sample\Symfony\MicroKernel('debug', true);    
    $app = new \Sample\Symfony\MicroKernel('dev', true);
endif;

$app->run();
